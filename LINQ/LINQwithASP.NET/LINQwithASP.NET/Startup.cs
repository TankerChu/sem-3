﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LINQwithASP.NET.Startup))]
namespace LINQwithASP.NET
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
