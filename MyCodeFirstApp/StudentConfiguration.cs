﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using MyCodeFirstApplication;

namespace MyCodeFirstApp
{
    public class StudentConfiguration : EntityTypeConfiguration<Student>
    {
        public StudentConfiguration()
        {
            this.Property(s => s.StudentName).IsRequired().HasMaxLength(50);
            this.HasOptional(s => s.Address).WithRequired(ad => ad.Student);
            this.Property(s => s.StudentName).IsConcurrencyToken();
        }
    }
}
