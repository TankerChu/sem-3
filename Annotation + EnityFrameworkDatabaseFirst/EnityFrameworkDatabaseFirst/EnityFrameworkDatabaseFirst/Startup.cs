﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EnityFrameworkDatabaseFirst.Startup))]
namespace EnityFrameworkDatabaseFirst
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
